import { LandingPage } from "./LandingPage";
import { LoginPage } from "./Login/LoginPage";
import { RegisterPage } from "./Register/RegisterPage";
import { Viewer } from "./Viewer/Viewer";

export { LandingPage, LoginPage, RegisterPage, Viewer };
